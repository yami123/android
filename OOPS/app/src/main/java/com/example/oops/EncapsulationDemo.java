package com.example.oops;

class Student
{
 private int rollno;
 private String name;
    //Getters and setters

public void setRollno(int r)
{
    rollno = r;
}
public int getRollno()
{
    return rollno;
}
}
public class EncapsulationDemo

{

    public static void main(String args[])
    {
        Student s1 = new Student();
        s1.setRollno(4);
        s1.setName("Navin");
        System.out.println(s1.getRollno());
        System.out.println(s1.getName());

    }
}



