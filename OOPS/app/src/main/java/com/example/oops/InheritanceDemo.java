package com.example.oops;
class Calculator    //super
{
    public int add(int i, int j)
    {
        return i + j;
    }
}
class CalcAdv extends Calculator
{
   public int sub(int i, int j)             //sub
   {
       return i-j;
   }
}



public class InheritanceDemo
{
    public static void main(String args[])

    {
        CalcAdv c1=new CalcAdv();
        int result1 = c1.add(5, 6);
        int result2 = c1.add(7,9);

        System.out.println(result1);
        System.out.println(result2);


    }
}



