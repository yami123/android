package com.example.myapplication134;
import java.util.Scanner;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.Scanner;


import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.myapplication134.databinding.ActivityMain2Binding;
import static java.lang.Thread.currentThread;
public class MainActivity2 extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMain2Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}

 class ThreadStopTest
{
    public static void main(String args[])

            throws InterruptedException
    {
        UserThread userThread = new UserThread();
        Thread thread = new Thread(userThread, "T1");
        thread.start();
        System.out.println(currentThread().getName() + " is stopping user thread");
        userThread.stop();
        Thread.sleep(5000);
        System.out.println(currentThread().getName() + " is finished now");
    }
}
class UserThread implements Runnable
{
    private boolean exit = false;

    public void run()
    {
        while(!exit)
        {
            System.out.println("The user thread is running");
        }
        System.out.println("The user thread is now stopped");
    }
    public void stop()
    {
        exit = true;
    }
}



class Book implements Comparable<Book> {
    @Override
    public int compareTo(Book o)
    {
        return 0;
    }

    int id;
    String name, author, publisher;
    int quantity;


    Book(int id, String name, String author, String publisher, int quantity)
    {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.quantity = quantity;
    }

    int CompareTo(Book b)
    {

        if (id > b.id)
        {
            return 1;
        }
        if(id<b.id)
        {
            return -1;


        }
        else
        {
            return 0;
        }


    }
}

class LinkedListExample
{
    public static void main(String args[])
    {
        Queue<Book> queue = new PriorityQueue<Book>();


        Book b1 = new Book(121, "Twillight", "Stephnie MAayor", "corloene", 200);
        Book b2 = new Book(124, "The God father", "Mario Puzo", "corloene", 400);
        Book b3 = new Book(254, "Narcos", "pablo escobar", "corleone", 600);

        queue.add(b1);
        queue.add(b2);
        queue.add(b3);

        System.out.println("Traversing the queue elements");
        for (Book b : queue)
        {
            System.out.println(b.id + "" + b.name + "" + b.author + "" + b.publisher + "" + b.quantity);
        }
        queue.peek();


        System.out.println("After removing one book record");

        for(Book b: queue)
        {
            System.out.println(b.id+""+b.name+""+b.author+""+b.publisher+""+b.quantity);

        }
    }
}

class TestCollection
{
    public static void main(String args[])
    {
        PriorityQueue<String> queue = new PriorityQueue<String>();
        queue.add("Rahul");
        queue.add("nayan");
        queue.add("mayank");
        queue.add("pushp");
        queue.add("nalin");

        System.out.println("head:" + queue.element());
        System.out.println("head:" + queue.peek());
        System.out.println("head:" + queue.element());

        System.out.println("iterating the queue elements");
        Iterator itr = queue.iterator();
        while (itr.hasNext()) ;
        {
        System.out.println(itr.next());
    }
    queue.remove();
    queue.poll();

System.out.println("After removing two elements");

Iterator<String> itr2=queue.iterator();

while(itr2.hasNext());
        {
            System.out.println(itr2.next());
        }







    }
}



class ArrayDequeExample
{
    public static void main(String args[])
    {
        Deque<String> deque=new ArrayDeque<String>();

        deque.add("Ravi");
        deque.add("Mayank");
        deque.add("neerav");

        for(String str: deque)
        {
            System.out.println(str);

        }
    }
}

class Book12
{
    int id;
    String name;
    String author;
    String publisher;
    int quantity;

    Book12(int id, String name, String author, String publisher, int quantity)
    {
        this.id = id;
        this.name=name;
        this.author=author;
        this.publisher=publisher;
        this.quantity=quantity;
    }
}
class ArrayDeque12
{
    public static void main(String args[])
    {
        Deque<Book12> set=new ArrayDeque<Book12>();

        Book12 b1=new Book12(102, "Twillight", "stephnie mayor", "corleone", 200);
        Book12 b2=new Book12(103, "The God father", "mario puzo", "corleone", 300 );
        Book12 b3=new Book12(104, "narcos", "ABC", "XYZ", 400);

        set.add(b1);
        set.add(b2);
        set.add(b3);

        for(Book12 b: set)
        {
            System.out.println(b.id+""+b.name+""+b.author+""+b.publisher+""+b.quantity);
        }

    }
}


class StringtoIntExample
{
    public static void main(String args[])
    {
        String s="200";
        int i = Integer.parseInt(s);
        System.out.println(i);

    }
}

class StringtoIntEXample1
{
    public static void main(String args[])
    {
        String s="200";
        int i=Integer.parseInt(s);

        System.out.println(s+100);
        System.out.println(i+100);
    }
}
 class StringtoIntegerExample12
 {
     public static void main(String args[])
     {
         String s="200";
         Integer i = Integer.valueOf(s);
         System.out.println(i);
     }
 }
class StringToIntegerExample123
{
    public static void main(String args[])
    {
//Declaring a string
        String s="200";
//converting String into Integer using Integer.valueOf() method
        Integer i=Integer.valueOf(s);
        System.out.println(i);
    }}

    class StringtoInteger34
    {
        public static void main(String args[])
        {
            String s="Hello";
            Integer i=Integer.parseInt(s);
            System.out.println(i);
        }
    }

class InttoString
{
    public static void main(String args[])
    {
        int i=200;
        String s=String.valueOf(i);
        System.out.println(s+100);
        System.out.println(i+100);
    }
}



 class InttoStringFormat
{
    public static void main(String args[]) {
        int i = 20;
        String s = String.format("%d", i);
    }

    }

    class Student implements Serializable

    {
        public static void main(String args[])
        {

        }

        int id;
        String name;

        Student(int id, String name)
        {
            this.id = id;
            this.name = name;
        }
    }

        class FileOutputStream123
        {
            public static void main(String args[])
            {
                try
                {
                    Student s1 = new Student(121, "Aarav");

                    FileOutputStream fout = new FileOutputStream("f.txt");
                    ObjectOutputStream out = new ObjectOutputStream(fout);
                    out.writeObject(s1);
                    out.flush();
                    out.close();

                    System.out.println("success");
                }
                catch(Exception e)
                {
                    System.out.println(e);
                }


            }
        }

class Deserialization
{
      public static void main(String args[])

      {
          try
          {
              ObjectInputStream in = new ObjectInputStream(new FileInputStream("f.txt"));

              Student s = (Student) in.readObject();

              System.out.println(s.id + "" + s.name);

              in.close();
          }
          catch(Exception e)
          {
              System.out.println(e);

          }

      }
}


class Bike1
{
    Bike1()
    {
        System.out.println("Bike is created");
    }
    public static void main(String args[])
    {
        Bike1 b=new Bike1();
    }

}

class Student2
{
    int id;
    String name;
    Student2(int i, String n)
    {
        id=i;
        name=n;
    }
    void display()
    {
        System.out.println(id+""+name);
    }


public static void main(String args[])
{
    Student2 s1 = new Student2(111, "ABC");
    Student2 s2 = new Student2(123, "XYZ");
    s1.display();
    s2.display();
}
}


class TestArray
{
    public static void main(String args[])
    {
        int a[]=new int[5];
                a[0]=10;
                a[1]=12;
                a[2]=15;
                a[3]=16;
                for(int i=0;i<a.length;i++)

                    System.out.println(a[i]);
                }

    }

    class TestArray1
    {
        static void min(int arr[])
        {
            int min=arr[0];
            for(int i=1;i<arr.length;i++)

                if(min>arr[i])
                    min=arr[i];
                System.out.println(min);
            }
            public static void main(String args[])
            {
                int a[]={33,4, 5,6};
                min(a);
        }
    }

    class TestArray2
    {
        public static void main(String args[])
        {
            int a[][] = {{1,3,4}, {3,4,5}};
            int b[][] = {{1,3,4}, {3,5,6}};
            int c[][] = new int[2][3];
            for(int i=0;i<2;i++)
            {
                for(int j=0;j<2;j++)
                {
                    c[i][j]=a[i][j]+b[i][j];
                    System.out.println(c[i][j]+"");

                }
                System.out.println();
            }
        }
    }

    class Enum
    {
        public enum season {WINTER, SPRING, SUMMER, FALL}

        public static void main(String args[])
        {
            for (season s : season.values())
            {
                System.out.println(s);
            }





            System.out.println("Value of WINTER is: " + season.valueOf("WINTER"));
            System.out.println("Index of WINTER is: " + season.valueOf("WINTER"));
            System.out.println("Index of SUMMER is: " + season.valueOf("SUMMER"));
        }
    }


enum Color
{
    RED, GREEN, BLUE;
}

 class Test
{
    // Driver method
    public static void main(String[] args)
    {
        Color c1 = Color.RED;
        System.out.println(c1);
    }
}

class EnumExample2
{
    enum Season{WINTER, SPRING, SUMMER, FALL};

    public static void main(String args[])
    {
        Season s = Season.WINTER;
        System.out.println(s);
    }

}

class c1
{
    public int x = 5;
    protected int y = 45;
    int z = 6;
     int a = 78;

    public void meth1()
    {
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(a);
    }
}

    class ch_start_access_modifiers
    {
        public static void main(String args[])
        {
            c1 c = new c1();
            System.out.println(c.x);
            System.out.println(c.y);
            System.out.println(c.z);
            System.out.println(c.a);
        }
    }


 class LinearSearchExample
 {
     public static int linearSearch(int[] arr, int key)
     {
         for (int i = 0; i < arr.length; i++)
         {
             if (arr[i] == key)
             {
                 return i;
             }
         }
         return -1;
     }

     public static void main(String a[])
     {
         int[] a1 = {10, 20, 30, 50, 70, 90};
         int key = 80;
         System.out.println(key + " is found at index: " + linearSearch(a1, key));
     }
 }


class LinearSearchExample2
{
    public static void main(String args[])
    {
        int c, n, search, array[];

        Scanner in = new Scanner(System.in);
        System.out.println("Enter number of elements");
        n = in.nextInt();
        array = new int[n];

        System.out.println("Enter those " + n + " elements");

        for (c = 0; c < n; c++)

            array[c] = in.nextInt();

        System.out.println("Enter value to find");
        search = in.nextInt();

        for (c = 0; c < n; c++)
        {
            if (array[c] == search)
            {
                System.out.println(search + " is present at location " + (c) + ".");
                break;
            }
        }
        if (c == n)

            System.out.println(search + " isn't present in array.");
    }
}



class BubbleSort
{
    static void print(int a[])
    {
        int n = a.length;
        int i;
        for(i=0;i<n;i++)
        {
            System.out.println(a[i]+"");
        }
    }
    static void bubblesort(int a[])
    {
        int n=a.length;
        int i,j, temp;
        for(i=0;i<n;i++)
        {
           for(j=i+1;j<n;i++)
           {
             if(a[i]<a[j])
             {

                 temp = a[j];
                 a[i] = a[j];
                 a[j + 1] = temp;
             }



        }
    }
}

public static void main(String args[])
        {
           int a[] = {34, 10, 24, 36, 17};
           BubbleSort b1=new BubbleSort();
            System.out.println("Before sorting array elements are - ");


            System.out.println();
            System.out.println("After sorting array elements are - ");
            b1.print(a);

        }
}

class BinarySearchExample1
{
    public static int binarySearch(int arr[], int first, int last, int key)
    {
        if (last >= first)
        {
            int mid = first + (last - first) / 2;
            if (arr[mid] == key)
            {
                return mid;
            }
            if (arr[mid] > key)
            {
                return binarySearch(arr, first, mid - 1, key);// left
            }
            else
                {
                return binarySearch(arr, mid + 1, last, key);//right
            }
        }
        return -1;
    }

    public static void main(String args[])
    {
        int arr[] = {10, 20, 30, 40, 50};
        int key = 50;
        int last = arr.length - 1;
        int result = binarySearch(arr, 0, last, key);
        if (result == -1)
            System.out.println("Element is not found!");
        else
            System.out.println("Element is found at index: " + result);
    }
}
 class SelectionSortExample2
{
    public static void main(String args[])
    {
        int size, i, j, temp;
        int arr[] = new int[50];
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter Array Size : ");
        size = scan.nextInt();

        System.out.print("Enter Array Elements : ");
        for(i=0; i<size; i++)
        {
            arr[i] = scan.nextInt();
        }

        System.out.print("Sorting Array using Selection Sort Technique..\n");
        for(i=0; i<size; i++)
        {
            for(j=i+1; j<size; j++)
            {
                if(arr[i] > arr[j])
                {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        System.out.print("Now the Array after Sorting is :\n");
        for(i=0; i<size; i++)
        {
            System.out.print(arr[i]+ "  ");
        }
    }
}

class Merge
{


    void merge(int a[], int beg, int mid, int end)
    {
        int i, j, k;
        int n1 = mid - beg + 1;
        int n2 = end - mid;

        /* temporary Arrays */
        int LeftArray[] = new int[n1];
        int RightArray[] = new int[n2];

        /* copy data to temp arrays */
        for (i = 0; i < n1; i++)
            LeftArray[i] = a[beg + i];
        for (j = 0; j < n2; j++)
            RightArray[j] = a[mid + 1 + j];

        i = 0;
        j = 0;
        k = beg;

        while (i < n1 && j < n2)
        {
            if(LeftArray[i] <= RightArray[j])
            {
                a[k] = LeftArray[i];
                i++;
            }
            else
            {
                a[k] = RightArray[j];
                j++;
            }
            k++;
        }
        while (i<n1)
        {
            a[k] = LeftArray[i];
            i++;
            k++;
        }

        while (j<n2)
        {
            a[k] = RightArray[j];
            j++;
            k++;
        }
    }

    void mergeSort(int a[], int beg, int end)
    {
        if (beg < end)
        {
            int mid = (beg + end) / 2;
            mergeSort(a, beg, mid);
            mergeSort(a, mid + 1, end);
            merge(a, beg, mid, end);
        }
    }

    
    void printArray(int a[], int n)
    {
        int i;
        for (i = 0; i < n; i++)
            System.out.print(a[i] + " ");
    }

    public static void main(String args[])
    {
        int a[] = { 11, 30, 24, 7, 31, 16, 39, 41 };
        int n = a.length;
        Merge m1 = new Merge();
        System.out.println("\nBefore sorting array elements are - ");
        m1.printArray(a, n);
        m1.mergeSort(a, 0, n - 1);
        System.out.println("\nAfter sorting array elements are - ");
        m1.printArray(a, n);
        System.out.println("");
    }

}

class TestArray12
{
    public static void main(String args[])
    {
        int a[] = new int[5];
        a[0] = 10;
        a[1] = 20;
        a[2] = 30;
        a[3] = 40;


        for (int i = 0; i < a.length; i++)

            System.out.println(a[i]);
    }

}
class TestDemo
{
    public static void main(String args[])
    {
        int a[]={33,44,5,6};
        for(int i=0;i<a.length;i++)
        {
            System.out.println(a[i]);
        }
    }
}

class TestArray23
{
    static void min(int arr[])
    {
        int min=arr[0];

        for (int i = 1; i<arr.length;i++)

            if (min > arr[i])
                min = arr[i];

                System.out.println(min);


            }

        public static void main (String args[])
        {
            int a[] = {33, 4, 5, 6};

            min(a);
        }
    }

    class TestArray3
    {
        public static void main(String args[])
        {
            int arr[][]={{1,2,3}, {2,4,5}, {4,4,5} };

            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    System.out.println(arr[i][j]+"");
                }
                System.out.println();
            }
        }
    }

class TestArray5
{
    public static void main(String args[])
    {
        int a[][]={{1,3,4}, {3,4,5}};
        int b[][]={{1,3,4}, {3,4,5}};
        int c[][]=new int[2][3];

        for(int i=0;i<2;i++)
        {
            for(int j=0;j<3;j++)
            {
                c[i][j] = a[i][j] + b[i][j];
                System.out.println(c[i][j] + "");
            }
            System.out.println();

            }
        }
    }

    class MatrixMultiplication
    {
        public static void main(String args[])
        {
            int a[][]={{1,1,1}, {3,4,5},{4,5,6}};
            int b[][]={{3,5,6}, {2,3,1}, {6,5,4}};
            int c[][]=new int[3][3];

            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    c[i][j] = 0;
                    for (int k = 0; k < 3; k++)
                    {
                        c[i][j] = a[i][k] * b[k][j];
                    }
                    System.out.println(c[i][j] + "");
                }
                System.out.println();


                    }

                }

            }


        class Enum123
        {
            public enum Season {WINTER, SPRING, FALL, SUMMER}

            public static void main(String args[])
            {
                for (Season s : Season.values())
                    System.out.println(s);
            }


        }

        enum Season{WINTER, SUMMER, SPRING, FALL}

        class EnumExample23
        {
            enum Season{WINTER, SUMMER, SPRING, FALL}
            public static void main(String args[])
            {
                Season s=Season.WINTER;
                System.out.println(s);
            }
        }

        class ExceptionExample
        {
            public static void main(String args[])
            {
                try
                {
                    int data = 100 / 0;

                }

                    catch(ArithmeticException e)
                    {
                        System.out.println(e);
                    }
                System.out.println("rest of the code");

                }
            }

            class MultipleCatchBlock
            {
                public static void main(String args[]) {
                    try {
                        int a[] = new int[5];






                        class Book implements Comparable<Book> {
                            @Override
                            public int compareTo(Book o) {
                                return 0;
                            }

                            int id;
                            String name, author, publisher;
                            int quantity;


                            Book(int id, String name, String author, String publisher, int quantity) {
                                this.id = id;
                                this.name = name;
                                this.author = author;
                                this.publisher = publisher;
                                this.quantity = quantity;
                            }

                            int CompareTo(Book b) {

                                if (id > b.id) {
                                    return 1;
                                }
                                if (id < b.id) {
                                    return -1;


                                } else {
                                    return 0;
                                }


                            }
                        }

                        class LinkedListExample
                        {
                            public static void main(String args[])
                            {
                                Queue<Book> queue = new PriorityQueue<Book>();


                                Book b1 = new Book(121, "Twillight", "Stephnie MAayor", "corloene", 200);
                                Book b2 = new Book(124, "The God father", "Mario Puzo", "corloene", 400);
                                Book b3 = new Book(254, "Narcos", "pablo escobar", "corleone", 600);

                                queue.add(b1);
                                queue.add(b2);
                                queue.add(b3);

                                System.out.println("Traversing the queue elements");
                                for (Book b : queue)
                                {
                                    System.out.println(b.id + "" + b.name + "" + b.author + "" + b.publisher + "" + b.quantity);
                                }
                                queue.peek();


                                System.out.println("After removing one book record");
                                for (Book b : queue)
                                {
                                    System.out.println(b.id + "" + b.name + "" + b.author + "" + b.publisher + "" + b.quantity);

                                }
                            }
                        }

                        class TestCollection
                        {
                            public static void main(String args[])
                            {
                                PriorityQueue<String> queue = new PriorityQueue<String>();
                                queue.add("Rahul");
                                queue.add("nayan");
                                queue.add("mayank");
                                queue.add("pushp");
                                queue.add("nalin");

                                System.out.println("head:" + queue.element());
                                System.out.println("head:" + queue.peek());
                                System.out.println("head:" + queue.element());

                                System.out.println("iterating the queue elements");
                                Iterator itr = queue.iterator();
                                while (itr.hasNext()) ;
                                {
                                    System.out.println(itr.next());
                                }
                                queue.remove();
                                queue.poll();

                                System.out.println("After removing two elements");
                                Iterator<String> itr2 = queue.iterator();
                                while (itr2.hasNext()) ;
                                {
                                    System.out.println(itr2.next());
                                }


                            }
                        }


                        class ArrayDequeExample
                        {
                            public static void main(String args[])
                            {
                                Deque<String> deque = new ArrayDeque<String>();

                                deque.add("Ravi");
                                deque.add("Mayank");
                                deque.add("neerav");

                                for (String str : deque)
                                {
                                    System.out.println(str);

                                }
                            }
                        }

                        class Book12
                        {
                            int id;
                            String name;
                            String author;
                            String publisher;
                            int quantity;

                            Book12(int id, String name, String author, String publisher, int quantity)
                            {
                                this.id = id;
                                this.name = name;
                                this.author = author;
                                this.publisher = publisher;
                                this.quantity = quantity;
                            }
                        }
                        class ArrayDeque12
                        {
                            public static void main(String args[])
                            {
                                Deque<Book12> set = new ArrayDeque<Book12>();

                                Book12 b1 = new Book12(102, "Twillight", "stephnie mayor", "corleone", 200);
                                Book12 b2 = new Book12(103, "The God father", "mario puzo", "corleone", 300);
                                Book12 b3 = new Book12(104, "narcos", "ABC", "XYZ", 400);

                                set.add(b1);
                                set.add(b2);
                                set.add(b3);

                                for (Book12 b : set)
                                {
                                    System.out.println(b.id + "" + b.name + "" + b.author + "" + b.publisher + "" + b.quantity);
                                }

                            }
                        }


                        class StringtoIntExample
                        {
                            public static void main(String args[])
                            {
                                String s = "200";
                                int i = Integer.parseInt(s);
                                System.out.println(i);

                            }
                        }

                        class StringtoIntEXample1
                        {
                            public static void main(String args[])
                            {
                                String s = "200";
                                int i = Integer.parseInt(s);

                                System.out.println(s + 100);
                                System.out.println(i + 100);
                            }
                        }
                        class StringtoIntegerExample12
                        {
                            public static void main(String args[])
                            {
                                String s = "200";
                                Integer i = Integer.valueOf(s);
                                System.out.println(i);
                            }
                        }
                        class StringToIntegerExample123
                        {
                            public static void main(String args[])
                            {
//Declaring a string
                                String s = "200";
//converting String into Integer using Integer.valueOf() method
                                Integer i = Integer.valueOf(s);
                                System.out.println(i);
                            }
                        }

                        class StringtoInteger34
                        {
                            public static void main(String args[])
                            {
                                String s = "Hello";
                                Integer i = Integer.parseInt(s);
                                System.out.println(i);
                            }
                        }

                        class InttoString {
                            public static void main(String args[])
                            {
                                int i = 200;
                                String s = String.valueOf(i);
                                System.out.println(s + 100);
                                System.out.println(i + 100);
                            }
                        }


                        class InttoStringFormat
                        {
                            public static void main(String args[])
                            {
                                int i = 20;
                                String s = String.format("%d", i);
                            }

                        }

                        class Student implements Serializable {
                            public static void main(String args[]) {

                            }

                            int id;
                            String name;

                            Student(int id, String name) {
                                this.id = id;
                                this.name = name;
                            }
                        }

                        class FileOutputStream123
                        {
                            public static void main(String args[])
                            {
                                try
                                {
                                    Student s1 = new Student(121, "Aarav");

                                    FileOutputStream fout = new FileOutputStream("f.txt");
                                    ObjectOutputStream out = new ObjectOutputStream(fout);
                                    out.writeObject(s1);
                                    out.flush();
                                    out.close();

                                    System.out.println("success");
                                }
                                catch (Exception e)
                                {
                                    System.out.println(e);
                                }


                            }
                        }

                        class Deserialization
                        {
                            public static void main(String args[])
                            {
                                try
                                {
                                    ObjectInputStream in = new ObjectInputStream(new FileInputStream("f.txt"));

                                    Student s = (Student) in.readObject();

                                    System.out.println(s.id + "" + s.name);

                                    in.close();
                                } catch (Exception e) {
                                    System.out.println(e);

                                }

                            }
                        }


                        class Bike1 {
                            Bike1() {
                                System.out.println("Bike is created");
                            }

                            public static void main(String args[]) {
                                Bike1 b = new Bike1();
                            }

                        }

                        class Student2 {
                            int id;
                            String name;

                            Student2(int i, String n) {
                                id = i;
                                name = n;
                            }

                            void display() {
                                System.out.println(id + "" + name);
                            }


                            public static void main(String args[]) {
                                Student2 s1 = new Student2(111, "ABC");
                                Student2 s2 = new Student2(123, "XYZ");
                                s1.display();
                                s2.display();
                            }
                        }


                        class TestArray {
                            public static void main(String args[]) {
                                int a[] = new int[5];
                                a[0] = 10;
                                a[1] = 12;
                                a[2] = 15;
                                a[3] = 16;
                                for (int i = 0; i < a.length; i++)

                                    System.out.println(a[i]);
                            }

                        }

                        class TestArray1 {
                            static void min(int arr[]) {
                                int min = arr[0];
                                for (int i = 1; i < arr.length; i++)

                                    if (min > arr[i])
                                        min = arr[i];
                                System.out.println(min);
                            }

                            public static void main(String args[]) {
                                int a[] = {33, 4, 5, 6};
                                min(a);
                            }
                        }

                        class TestArray2
                        {
                            public static void main(String args[])
                            {
                                int a[][] = {{1, 3, 4}, {3, 4, 5}};
                                int b[][] = {{1, 3, 4}, {3, 5, 6}};
                                int c[][] = new int[2][3];
                                for (int i = 0; i < 2; i++)
                                {
                                    for (int j = 0; j < 2; j++)
                                    {
                                        c[i][j] = a[i][j] + b[i][j];
                                        System.out.println(c[i][j] + "");

                                    }
                                    System.out.println();
                                }
                            }
                        }

                        class Enum {
                            public enum season {WINTER, SPRING, SUMMER, FALL}

                            public static void main(String args[]) {
                                for (season s : season.values()) {
                                    System.out.println(s);
                                }


                                System.out.println("Value of WINTER is: " + season.valueOf("WINTER"));
                                System.out.println("Index of WINTER is: " + season.valueOf("WINTER"));
                                System.out.println("Index of SUMMER is: " + season.valueOf("SUMMER"));
                            }
                        }


                        enum Color {
                            RED, GREEN, BLUE;
                        }

                        class Test {
                            // Driver method
                            public static void main(String[] args) {
                                Color c1 = Color.RED;
                                System.out.println(c1);
                            }
                        }

                        class EnumExample2 {
                            enum Season {WINTER, SPRING, SUMMER, FALL}

                            ;

                            public static void main(String args[]) {
                                Season s = Season.WINTER;
                                System.out.println(s);
                            }

                        }

                        class c1
                        {
                            public int x = 5;
                            protected int y = 45;
                            int z = 6;
                            int a = 78;

                            public void meth1()
                            {
                                System.out.println(x);
                                System.out.println(y);
                                System.out.println(z);
                                System.out.println(a);
                            }
                        }

                        class ch_start_access_modifiers {
                            public static void main(String args[]) {
                                c1 c = new c1();
                                System.out.println(c.x);
                                System.out.println(c.y);
                                System.out.println(c.z);
                                System.out.println(c.a);
                            }
                        }


                        class LinearSearchExample
                        {
                            public static int linearSearch(int[] arr, int key)
                            {
                                for (int i = 0; i < arr.length; i++)
                                {
                                    if (arr[i] == key)
                                    {
                                        return i;
                                    }
                                }
                                return -1;
                            }

                            public static void main(String a[])
                            {
                                int[] a1 = {10, 20, 30, 50, 70, 90};
                                int key = 80;
                                System.out.println(key + " is found at index: " + linearSearch(a1, key));
                            }
                        }


                        class LinearSearchExample2
                        {
                            public static void main(String args[]) {
                                int c, n, search, array[];

                                Scanner in = new Scanner(System.in);
                                System.out.println("Enter number of elements");
                                n = in.nextInt();
                                array = new int[n];

                                System.out.println("Enter those " + n + " elements");

                                for (c = 0; c < n; c++)

                                    array[c] = in.nextInt();

                                System.out.println("Enter value to find");
                                search = in.nextInt();

                                for (c = 0; c < n; c++) {
                                    if (array[c] == search) {
                                        System.out.println(search + " is present at location " + (c) + ".");
                                        break;
                                    }
                                }
                                if (c == n)

                                    System.out.println(search + " isn't present in array.");
                            }
                        }


                        class BubbleSort {
                            static void print(int a[]) {
                                int n = a.length;
                                int i;
                                for (i = 0; i < n; i++) {
                                    System.out.println(a[i] + "");
                                }
                            }

                            static void bubblesort(int a[]) {
                                int n = a.length;
                                int i, j, temp;
                                for (i = 0; i < n; i++) {
                                    for (j = i + 1; j < n; i++) {
                                        if (a[i] < a[j]) {

                                            temp = a[j];
                                            a[i] = a[j];
                                            a[j + 1] = temp;
                                        }


                                    }
                                }
                            }

                            public static void main(String args[]) {
                                int a[] = {34, 10, 24, 36, 17};
                                BubbleSort b1 = new BubbleSort();
                                System.out.println("Before sorting array elements are - ");


                                System.out.println();
                                System.out.println("After sorting array elements are - ");
                                b1.print(a);

                            }
                        }

                        class BinarySearchExample1
                        {
                            public static int binarySearch(int arr[], int first, int last, int key)
                            {
                                if (last >= first)
                                {
                                    int mid = first + (last - first) / 2;
                                    if (arr[mid] == key)
                                    {
                                        return mid;
                                    }
                                    if (arr[mid] > key)
                                    {
                                        return binarySearch(arr, first, mid - 1, key);// left
                                    }
                                    else
                                        {
                                        return binarySearch(arr, mid + 1, last, key);//right
                                    }
                                }
                                return -1;
                            }

                            public static void main(String args[]) {
                                int arr[] = {10, 20, 30, 40, 50};
                                int key = 50;
                                int last = arr.length - 1;
                                int result = binarySearch(arr, 0, last, key);
                                if (result == -1)
                                    System.out.println("Element is not found!");
                                else
                                    System.out.println("Element is found at index: " + result);
                            }
                        }
                        class SelectionSortExample2
                        {
                            public static void main(String args[])
                            {
                                int size, i, j, temp;
                                int arr[] = new int[50];
                                Scanner scan = new Scanner(System.in);

                                System.out.print("Enter Array Size : ");
                                size = scan.nextInt();

                                System.out.print("Enter Array Elements : ");
                                for (i = 0; i < size; i++) {
                                    arr[i] = scan.nextInt();
                                }

                                System.out.print("Sorting Array using Selection Sort Technique..\n");
                                for (i = 0; i < size; i++) {
                                    for (j = i + 1; j < size; j++) {
                                        if (arr[i] > arr[j]) {
                                            temp = arr[i];
                                            arr[i] = arr[j];
                                            arr[j] = temp;
                                        }
                                    }
                                }

                                System.out.print("Now the Array after Sorting is :\n");
                                for (i = 0; i < size; i++) {
                                    System.out.print(arr[i] + "  ");
                                }
                            }
                        }

                        class Merge {


                            void merge(int a[], int beg, int mid, int end) {
                                int i, j, k;
                                int n1 = mid - beg + 1;
                                int n2 = end - mid;

                                /* temporary Arrays */
                                int LeftArray[] = new int[n1];
                                int RightArray[] = new int[n2];

                                /* copy data to temp arrays */
                                for (i = 0; i < n1; i++)
                                    LeftArray[i] = a[beg + i];
                                for (j = 0; j < n2; j++)
                                    RightArray[j] = a[mid + 1 + j];

                                i = 0;
                                j = 0;
                                k = beg;

                                while (i < n1 && j < n2) {
                                    if (LeftArray[i] <= RightArray[j]) {
                                        a[k] = LeftArray[i];
                                        i++;
                                    } else {
                                        a[k] = RightArray[j];
                                        j++;
                                    }
                                    k++;
                                }
                                while (i < n1) {
                                    a[k] = LeftArray[i];
                                    i++;
                                    k++;
                                }

                                while (j < n2) {
                                    a[k] = RightArray[j];
                                    j++;
                                    k++;
                                }
                            }

                            void mergeSort(int a[], int beg, int end) {
                                if (beg < end) {
                                    int mid = (beg + end) / 2;
                                    mergeSort(a, beg, mid);
                                    mergeSort(a, mid + 1, end);
                                    merge(a, beg, mid, end);
                                }
                            }


                            void printArray(int a[], int n) {
                                int i;
                                for (i = 0; i < n; i++)
                                    System.out.print(a[i] + " ");
                            }

                            public static void main(String args[])
                            {
                                int a[] = {11, 30, 24, 7, 31, 16, 39, 41};
                                int n = a.length;
                                Merge m1 = new Merge();
                                System.out.println("\nBefore sorting array elements are - ");
                                m1.printArray(a, n);
                                m1.mergeSort(a, 0, n - 1);
                                System.out.println("\nAfter sorting array elements are - ");
                                m1.printArray(a, n);
                                System.out.println("");
                            }

                        }

                        class TestArray12
                        {
                            public static void main(String args[])
                            {
                                int a[] = new int[5];
                                a[0] = 10;
                                a[1] = 20;
                                a[2] = 30;
                                a[3] = 40;


                                for (int i = 0; i < a.length; i++)

                                    System.out.println(a[i]);
                            }

                        }
                        class TestDemo
                        {
                            public static void main(String args[])
                            {
                                int a[] = {33, 44, 5, 6};
                                for (int i = 0; i < a.length; i++) {
                                    System.out.println(a[i]);
                                }
                            }
                        }

                        class TestArray23 {
                            static void min(int arr[]) {
                                int min = arr[0];

                                for (int i = 1; i < arr.length; i++)

                                    if (min > arr[i])
                                        min = arr[i];

                                System.out.println(min);


                            }

                            public static void main(String args[])
                            {
                                int a[] = {33, 4, 5, 6};

                                min(a);
                            }
                        }

                        class TestArray3
                        {
                            public static void main(String args[])
                            {
                                int arr[][] = {{1, 2, 3}, {2, 4, 5}, {4, 4, 5}};

                                for (int i = 0; i < 3; i++) {
                                    for (int j = 0; j < 3; j++) {
                                        System.out.println(arr[i][j] + "");
                                    }
                                    System.out.println();
                                }
                            }
                        }

                        class TestArray5 {
                            public static void main(String args[]) {
                                int a[][] = {{1, 3, 4}, {3, 4, 5}};
                                int b[][] = {{1, 3, 4}, {3, 4, 5}};
                                int c[][] = new int[2][3];

                                for (int i = 0; i < 2; i++) {
                                    for (int j = 0; j < 3; j++) {
                                        c[i][j] = a[i][j] + b[i][j];
                                        System.out.println(c[i][j] + "");
                                    }
                                    System.out.println();

                                }
                            }
                        }

                        class MatrixMultiplication
                        {
                            public static void main(String args[])
                            {
                                int a[][] = {{1, 1, 1}, {3, 4, 5}, {4, 5, 6}};
                                int b[][] = {{3, 5, 6}, {2, 3, 1}, {6, 5, 4}};
                                int c[][] = new int[3][3];

                                for (int i = 0; i < 3; i++) {
                                    for (int j = 0; j < 3; j++) {
                                        c[i][j] = 0;
                                        for (int k = 0; k < 3; k++) {
                                            c[i][j] = a[i][k] * b[k][j];
                                        }
                                        System.out.println(c[i][j] + "");
                                    }
                                    System.out.println();


                                }

                            }

                        }


                        class Enum123
                        {
                            public enum Season {WINTER, SPRING, FALL, SUMMER}

                            public static void main(String args[])
                            {
                                for (Season s : Season.values())
                                    System.out.println(s);
                            }


                        }


                        class EnumExample23
                        {
                            enum Season {WINTER, SUMMER, SPRING, FALL}

                            public static void main(String args[])
                            {
                                Season s = Season.WINTER;
                                System.out.println(s);
                            }
                        }

                        class ExceptionExample
                        {
                            public static void main(String args[])
                            {
                                try
                                {
                                    int data = 100 / 0;

                                }
                                catch (ArithmeticException e)
                                {
                                    System.out.println(e);
                                }
                                System.out.println("rest of the code");

                            }
                        }


class ExceptionDemo
{
    public static void main(String args[])
    {
        try
        {
            catch
        }
    }
















































