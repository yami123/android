package com.example.exceptiondemo12;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}


 class ExceptionDemo
{
    public static void main(String args[])
    {
        Integer a = 12;

        try
        {
            String s = "Hi";

            a=Integer.parseInt("Error");



        }

        catch (Exception e)


        {

            System.out.println(a);
            System.out.println(e);

            System.out.println("Hi");
        }
    }
}



class RightTrianglePattern1
{
    public static void main(String args[])
    {
        int i, j, row=3;

        for(i=0; i<row; i++)
        {
            for(j=0; j<=i; j++)
            {
                System.out.println("* ");
            }
            System.out.println();
        }
    }
}

class RightTrianglePattern
{
    public static void main(String args[])
    {
//i for rows and j for columns
//row denotes the number of rows you want to print
        int i, j, row=3;
//outer loop for rows
        for(i=0; i<row; i++)
        {
//inner loop for columns
            for(j=0; j<=i; j++)
            {
//prints stars
                System.out.print("* ");
            }
//throws the cursor in a new line after printing each line
            System.out.println();
        }
    }
}