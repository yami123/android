package com.example.exceptiondemo12;

class LeftDownPattern

    {
        public static void main(String args[])
        {
            int row=3;
            for (int i= row-1; i>= 0; i--)
            {
                for (int j=0; j<=i; j++)
                {


                    System.out.print("* ");
                }
                System.out.println("");
            }
        }
    }

    class RightDownMirrorPattern
    {
        public static void main(String args[])
        {
            int row=3;
            for(int i=row; i>=1; i--)
            {
                for(int j=row; j>i; j--)
                {
                    System.out.print(" ");
                }
                for(int k=1;k<=i; k++)
                {
                    System.out.print("*");
                }
                System.out.println(" ");

            }
        }
    }

